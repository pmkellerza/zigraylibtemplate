# Zig Raylib Template
Using Zig 0.11-0.dev.3278
Raylib 4.6-dev

## Getting Started
Import the project template by using the following below:
N.B. <projectfolder> will be replaced by your name for the folder you want to clone.

```
git clone --recurse-submodules https://gitlab.com/pmkellerza/zigraylibtemplate.git <projectfolder>
```

## RayGUI
Copy raygui.h from libs/raygui/src to libs/raylib/src


## Customize build.zig
Change the "zig-rayl-template" to your own application name
```
pub const APP_NAME = "zig-raylib-template";
```
## constants.zig
#### src/contants.zig
This zig contents constants used by the application.
WIDTH and HEIGHT are used for the raylib window size.
TTITLE is used for the window title.
Change accordingly.
```
pub const WIDTH: i32 = 1280;
pub const HEIGHT: i32 = 960;
pub const TITLE: [*:0]const u8 = "My Zig Raylib Game";
```
### run locally

```sh
zig build run
```

### build for host os and architecture

```sh
zig build -Doptimize=ReleaseSmall
```

