const std = @import("std");

const c = @import("../constants.zig");
//Raylib
const rl = @cImport({
    @cInclude("raylib.h");
});
//raygui
const rlgui = @cImport({
    @cInclude("raygui.h");
});

const Entities = @import("../entities/entities.zig");
const Components = @import("../components/components.zig");
const helper = @import("helper.zig");

pub const Game = struct {
    const Self = @This();

    pub fn init() !Self {
        return Self{};
    }

    pub fn deinit(self: *Self) void {
        _ = self;
    }

    pub fn run(self: *Self) !void {
        try self.update();

        try self.render();
    }

    pub fn update(self: *Self) !void {
        _ = self;
    }

    pub fn render(self: *Self) !void {
        _ = self;

        rl.ClearBackground(rl.RAYWHITE);

        rl.DrawText("My First Zig Raylib Window", 499, c.HEIGHT / 2, 20, rl.BLUE);

        const btnWidth = 200;
        const btnHeight = 100;

        const btn = rlgui.GuiButton(.{ .x = c.WIDTH / 2 - btnWidth / 2, .y = c.HEIGHT / 2 + btnHeight, .width = btnWidth, .height = btnHeight }, "press me!");

        if (helper.getBool(btn)) {
            std.debug.print("pressed\n", .{});
        }
    }
};
