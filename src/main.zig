const std = @import("std");
const c = @import("constants.zig");
//Raylib
const rl = @cImport({
    @cInclude("raylib.h");
});

const GenIndexMap = @import("genindexmap");

const Game = @import("gamemode/game.zig").Game;

pub fn main() !void {

    //init Window
    rl.InitWindow(c.WIDTH, c.HEIGHT, c.TITLE);
    defer rl.CloseWindow();

    rl.SetTargetFPS(60);

    var game = try Game.init();

    while (!rl.WindowShouldClose()) {
        rl.BeginDrawing();
        defer rl.EndDrawing();

        try game.run();
    }

    game.deinit();
}
